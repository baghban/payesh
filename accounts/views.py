from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from django.core.exceptions import PermissionDenied
from django.views import View
from django.contrib.auth import authenticate, login
from django.utils.decorators import method_decorator
import re
from functools import wraps
from ratelimit.decorators import ratelimit
from django_ajax.decorators import ajax
from .forms import RegisterForm, LoginForm
from django.contrib.auth import get_user_model
from .devices import EmailDevice, SMSDevice

UserModel = get_user_model()
PHONE_PATTERN = r'^0?9\d{9}$'
EMAIL_PATTERN = r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$'


def verified_device_required(device_field):
    def decorator(function):
        @wraps(function)
        def _wrapped(request, *args, **kwargs):
            if bool(re.match(EMAIL_PATTERN, request.POST.get(device_field))):
                device = EmailDevice(request, request.POST.get(device_field))
                email = request.POST.get(device_field)
                phone_number = None
            elif bool(re.match(PHONE_PATTERN, request.POST.get(device_field))):
                device = SMSDevice(request, request.POST.get(device_field))
                phone_number = request.POST.get(device_field)
                email = None
            else:
                raise PermissionDenied
            if device.is_confirmed:
                return function(request, *args, phone_number=phone_number, email=email, **kwargs)
            else:
                raise PermissionDenied
        return _wrapped
    return decorator

@method_decorator(verified_device_required('phone_email'), name='post')
class RegisterView(View):
    template = 'accounts/register.html'
    REGISTER_AUTO_LOGIN = getattr('settings', 'REGISTER_AUTO_LOGIN', True)
    
    def get(self, request):
        form = RegisterForm()
        return render(request, self.template, {'form': form})

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        data['phone_number'] = kwargs['phone_number']
        data['email'] = kwargs['email']
        backend = None
        errors={}
        form = RegisterForm(data)
        success = form.is_valid()
        if success:
            try:
                user = form.save(commit=False)
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('raw_password')
                phone_number = form.cleaned_data.get('phone_number')
                email = form.cleaned_data.get('email')
                user.set_password(raw_password)
                user.save()
                if self.REGISTER_AUTO_LOGIN:
                    # return redirect('login')
                    return LoginView.login(request, request.POST.get('phone_email'), raw_password)
            except Exception as e:
                success = False
                return redirect('register')
        else:
            return redirect('register')

class LoginView(View):
    template = 'accounts/login.html'
    
    def get(self, request):
        form = LoginForm()
        return render(request, self.template, {'form': form})

    # @ratelimit(key='ip', rate='5/m', block=True)
    @ratelimit(key='ip', rate='1/s', block=True)
    def post(self, request):
        data = request.POST.copy()
        phone_email = request.POST.get('phone_email')
        backend = None
        errors={}
        if bool(re.match(PHONE_PATTERN, phone_email)):
            data['phone_number'] = phone_email
            data['email'] = None
            backend = 'accounts.backends.PhoneBackend'
        elif bool(re.match(EMAIL_PATTERN, phone_email)):
            data['email'] = phone_email
            data['phone_number'] = None
            backend = 'accounts.backends.EmailBackend'
        form = LoginForm(data)
        success = form.is_valid()
        if success:
            raw_password = form.cleaned_data.get('raw_password')
            phone_number = form.cleaned_data.get('phone_number')
            email = form.cleaned_data.get('email')
            try:
                user = authenticate(phone_number=phone_number, email=email, password=raw_password)
                if user:
                    login(request, user, backend)
                    success = True
                    return redirect('home')
                else:
                    return redirect('login')
            except:
                success = False
                return redirect('login')
        else:
            return redirect('login')

    @staticmethod
    def login(request, phone_email, raw_password):
        data={}
        if bool(re.match(PHONE_PATTERN, phone_email)):
            data['phone_number'] = phone_email
            data['email'] = None
            backend = 'accounts.backends.PhoneBackend'
        elif bool(re.match(EMAIL_PATTERN, phone_email)):
            data['email'] = phone_email
            data['phone_number'] = None
            backend = 'accounts.backends.EmailBackend'
        data['raw_password'] = raw_password
        data['phone_email'] = phone_email
        form = LoginForm(data)
        success = form.is_valid()
        if success:
            raw_password = form.cleaned_data.get('raw_password')
            phone_number = form.cleaned_data.get('phone_number')
            email = form.cleaned_data.get('email')
            try:
                user = authenticate(phone_number=phone_number, email=email, password=raw_password)
                if user:
                    login(request, user, backend)
                    success = True
                    return redirect('home')
                else:
                    return redirect('login')
            except:
                success = False
                return redirect('login')
        else:
            return redirect('login')


@require_http_methods(['POST'])
@ajax
def validate_phone_email(request):
    result = {'success': True, 'message':''}
    phone_pattern = r'^0?9\d{9}$'
    email_pattern = r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$'
    phone_email = request.POST.get('phone_email')
    if bool(re.match(phone_pattern, phone_email)):
        user = UserModel.objects.filter(phone_number=phone_email)
        if user:
            result['success'] = False
            result['message'] = 'این شماره موبایل در دسترس نیست'
    elif bool(re.match(email_pattern, phone_email)):
        user = UserModel.objects.filter(email=phone_email)
        if user:
            result['success'] = False
            result['message'] = 'این ایمیل در دسترس نیست'
    else:
        result['success'] = False
        result['message'] = 'ایمیل یا شماره تلفن معتبر وارد کنید'
    return JsonResponse(result)


@require_http_methods(['POST'])
@ajax
@ratelimit(key='ip', rate='20/h', block=True)
def send_token(request):
    result = {'success': False, 'message':''}
    phone_email = request.POST.get('phone_email')
    if bool(re.match(PHONE_PATTERN, phone_email)):
        device = SMSDevice(request, phone_email)
    elif bool(re.match(EMAIL_PATTERN, phone_email)):
        device = EmailDevice(request, phone_email)
    else:
        result['success'] = False
        return JsonResponse(result)
    try:
        device.generate_challenge()
        result['success'] = True
    except Exception as e:
        result['success'] = False
    finally:
        return JsonResponse(result)
    

@require_http_methods(['POST'])
@ajax
def validate_token(request):
    result = {'success': False, 'message':''}
    pincode = request.POST.get('pincode')
    phone_email = request.POST.get('phone_email')
    if bool(re.match(PHONE_PATTERN, phone_email)):
        device = SMSDevice(request, phone_email)
    elif bool(re.match(EMAIL_PATTERN, phone_email)):
        device = EmailDevice(request, phone_email)
    else:
        result['success'] = False
        return JsonResponse(result)
    result['success'] = device.verify_token(pincode)
    return JsonResponse(result)
