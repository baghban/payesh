from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
import re

UserModel = get_user_model()


class RegisterForm(forms.ModelForm):
    phone_email = forms.RegexField(regex=r'(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)')
    phone_email.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'شماره همراه یا ایمیل',
        'data-parsley-group': 'username',
        'pattern': '(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)',
        'data-parsley-pattern': '(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)',
        'data-parsley-required': '',
        'data-parsley-remote': '',
        'data-parsley-remote-validator': 'uniqueusername',
        'data-parsley-remote-options': '{ "type": "POST", "dataType": "json"}',
        'data-parsley-remote-message': 'این موبایل یا ایمیل در دسترس نیست',
        'data-parsley-required-message': "شماره موبایل یا ایمیل خود را وارد کنید",
        'data-parsley-pattern-message': "یک شماره موبایل یا ایمیل معتبر وارد کنید",
        'autocomplete': "off"
    })
    # phone_email.widget.attrs.update(__parsley_config)
    pincode = forms.CharField()
    pincode.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'کد دریافتی',
        'data-parsley-group': 'pincode',
        'data-parsley-required': '',
        'data-parsley-type': "digits",
        'data-parsley-minlength': '6',
        'data-parsley-maxlength': '6',
        'data-parsley-remote': '',
        'data-parsley-remote-validator': 'correcttoken',
        'data-parsley-remote-options': '{ "type": "POST", "dataType": "json"}',
        'data-parsley-required-message': 'کد دریافت کرده را وارد کنید',
        'data-parsley-remote-message': 'کد وارد شده درست نیست',
        'data-parsley-type-message': 'کد تایید فقط از رقم تشکیل شده است',
        'data-parsley-minlength-message': 'یک شماره شش رقمی وارد کنید',
        'data-parsley-maxlength-message': 'یک شماره شش رقمی وارد کنید',
        'autocomplete': "off"
        })
    raw_password = forms.CharField(widget=forms.PasswordInput)
    raw_password.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'گدزواژه',
        'data-parsley-group': 'password',
        'data-parsley-required': '',
        'data-parsley-minlength': '5',
        'data-parsley-maxlength': '100',
        'data-parsley-required-message': 'گذرواژه را وارد کنید',
        'data-parsley-minlength-message': 'گذرواژه باید دست کم پنج حرف باشد',
        'data-parsley-maxlength-message': 'گذرواژه باید دست پر صد حرف باشد',
    })
            
    class Meta:
        model = UserModel
        fields = ['first_name', 'last_name', 'phone_number', 'email']
        widgets = {
            'first_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'نام',
                'data-parsley-group': 'name',
                'data-parsley-required': '',
                'data-parsley-minlength': '3',
                'data-parsley-maxlength': '50',
                'data-parsley-pattern': '^[\040\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC]+$',
                'data-parsley-required-message': 'نام خود را وارد کنید',
                'data-parsley-minlength-message': 'نام نباید کمتر از سه حرف باشد',
                'data-parsley-maxlength-message': 'نام نباید بیشتر از پنجاه حرف باشد',
                'data-parsley-pattern-message': 'نام را به فارسی وارد کنید',
                'autocomplete': "off"
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'نام خانوادگی',
                'data-parsley-group': 'name',
                'data-parsley-required': '',
                'data-parsley-minlength': '3',
                'data-parsley-maxlength': '50',
                'data-parsley-pattern': '^[\040\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC]+$',
                'data-parsley-required-message': 'نام خانوادگی خود را وارد کنید',
                'data-parsley-minlength-message': 'نام خانوادگی نباید کمتر از سه حرف باشد',
                'data-parsley-maxlength-message': 'نام خانوادگی نباید بیشتر از پنجاه حرف باشد',
                'data-parsley-pattern-message': 'نام خانوادگی را به فارسی وارد کنید',
                'autocomplete': "off"
            }),
        }


class LoginForm(forms.Form):
    phone_email = forms.RegexField(regex=r'(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)')
    phone_email.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'شماره همراه یا ایمیل',
        'pattern': '(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)',
        'data-parsley-pattern': '(^0?9\d{9}$)|(^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$)',
        'data-parsley-required': '',
        'data-parsley-remote-options': '{ "type": "POST", "dataType": "json"}',
        'data-parsley-required-message': "شماره موبایل یا ایمیل خود را وارد کنید",
        'data-parsley-pattern-message': "یک شماره موبایل یا ایمیل معتبر وارد کنید",
        'autocomplete': "off"
    })
    raw_password = forms.CharField(widget=forms.PasswordInput)
    raw_password.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'گدزواژه',
        'data-parsley-required': '',
    })
    phone_number = forms.CharField(required=False)
    email = forms.CharField(required=False)
