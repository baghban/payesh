$(document).ready(function() {

    $('input:text, input:password').keypress(function(event) {
        var key = event.which;
        if (key == 13) // the enter key code
        {
            event.preventDefault();
            $(this).parent().find('button').click();
        }
    });
    $('input:text, input:password').focus(function() {
        $(this).select();
    });

    var parsley_config = {
        errorClass: 'is-invalid',
        successClass: 'is-valid',
        errorsContainer: function(ParsleyField) {
            var $err = ParsleyField.$element.closest('.form-group')
            return $err;
        },
        errorsWrapper: '<span class="invalid-feedback"></span>',
        errorTemplate: '<li></li>',
        trigger: 'keyup',
        focus: 'first'
    }

    $('#register-form').parsley(parsley_config);
    // $('#register-form').unbind('submit'); //cancel validation on submit
    $('.eye').click(function() {
        if ($('#id_raw_password').attr('type') == 'password') {
            $(this).removeClass('eye-visible').addClass('eye-invisible');
            $('#id_raw_password').attr('type', 'text');
        } else {
            $(this).removeClass('eye-invisible').addClass('eye-visible');
            $('#id_raw_password').attr('type', 'password');
        }
    })

});