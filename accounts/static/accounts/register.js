$(document).ready(function() {
    var form = $("#register-form").show();
    window.Parsley.addAsyncValidator('uniqueusername', function(xhr) {
        var result = xhr.responseJSON;
        var content = JSON.parse(result.content);
        if (content.success == true)
            return true;
        else
            return false;
    }, '/validatephoneemail', {
        "data": {
            "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
        }
    });
    window.Parsley.addAsyncValidator('correcttoken', function(xhr) {
        var result = xhr.responseJSON;
        var content = JSON.parse(result.content);
        if (content.success == true)
            return true;
        else
            return false;
    }, '/validatetoken', {
        "data": {
            "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
            "phone_email": function() { return $('#id_phone_email').val() }
        }
    });

    function change_focus(event, currentIndex) {
        if (currentIndex == 0) {
            $('input[name="phone_email"]').focus();
        }
        switch (currentIndex) {
            case 0:
                $('input[name="phone_email"]').focus();
                break;
            case 1:
                $('input[name="pincode"]').focus();
                break;
            case 2:
                $('input[name="raw_password"]').focus();
                break;
            case 3:
                $('input[name="first_name"]').focus();
                break;
        }
    }
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enablePagination: false,
        forceMoveForward: true,
        onInit: function(event, currentIndex) {
            change_focus(event, currentIndex);
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            switch (currentIndex) {
                case 0:
                    return $('#register-form').parsley().whenValidate({ group: 'username' });
                    break;
                case 1:
                    return $('#register-form').parsley().whenValidate({ group: 'pincode' });
                    break;
                case 2:
                    return $('#register-form').parsley().whenValidate({ group: 'password' });
                    break;
            }
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            change_focus(event, currentIndex);
            if (currentIndex === 1 && priorIndex === 0) {
                $.ajax({
                    type: 'POST',
                    url: '/sendtoken',
                    data: {
                        csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
                        phone_email: $('#id_phone_email').val()
                    },
                    dataType: 'json',
                    success: function(result) {

                    }
                })
            }
        },
        onFinishing: function(event, currentIndex) {
            return true;
            return $('#register-form').parsley().whenValidate({ group: 'name' });
        },
        onFinished: function(event, currentIndex) {
            $('#register-form').submit();
        }
    });
    $('[name="next"]').click(function() {
        $(".wizard").steps("next");
    });
    $('[name="finish"]').click(function() {
        $(".wizard").steps("finish");
    });
    $('input:text, input:password').keypress(function(event) {
        var key = event.which;
        if (key == 13) // the enter key code
        {
            event.preventDefault();
            $(this).parent().find('button').click();
        }
    });
    $('input:text, input:password').focus(function() {
        $(this).select();
    });

    var parsley_config = {
        errorClass: 'is-invalid',
        successClass: 'is-valid',
        errorsContainer: function(ParsleyField) {
            var $err = ParsleyField.$element.closest('.form-group')
            return $err;
        },
        errorsWrapper: '<span class="invalid-feedback"></span>',
        errorTemplate: '<li></li>',
        trigger: 'keyup',
        focus: 'first'
    }

    $('#register-form').parsley(parsley_config);
    $('#register-form').unbind('submit'); //cancel validation on submit
    $('#reset-form').click(function() {
        $("#register-form")[0].reset();
        $('input:text, input:password').each(function() {
            $(this).parsley(parsley_config).reset();
        });
        $(".wizard").steps("reset");
    })
    $('.eye').click(function() {
        if ($('#id_raw_password').attr('type') == 'password') {
            $(this).removeClass('eye-visible').addClass('eye-invisible');
            $('#id_raw_password').attr('type', 'text');
        } else {
            $(this).removeClass('eye-invisible').addClass('eye-visible');
            $('#id_raw_password').attr('type', 'password');
        }
    })

});