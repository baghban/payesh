from django.core.mail import send_mail
# from django.contrib.sites.models import Site
# from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone
from django.conf import settings
import secrets
import datetime
from sms import Api

class Device(object):

    def __init__(self, request, *args, **kwargs):
        self.request = request

    class Meta(object):
        abstract = True

    def generate_challenge(self):
        raise NotImplementedError

    def verify_token(self, request, token):
        raise NotImplementedError
    
    @property
    def confirmed(self):
        raise NotImplementedError        


class EmailDevice(Device):

    MESSAGE_TEMPLATE = 'کاربر گرامی! کد تایید ایمیل شما در {0}، {1} می‌باشد.'
    SUBJECT = 'کد تایید ایمیل شما {} می‌باشد'
    DIGITS = getattr(settings, 'EMAIL_TOKEN_DIGITS', 6)
    SENDER = getattr(settings, "EMAIL_TOKEN_SENDER")
    EMAIL_TOKEN_EXPIRE_AFTER = getattr(settings, "EMAIL_TOKEN_EXPIRE_AFTER", 300)
    # SEND_TOLERANCE = getattr(settings, 'EMAIL_TOKEN_SEND_TOLERANCE', 5)

    def __init__(self, request, address, *args, **kwargs):
        self.address = address
        super(EmailDevice, self).__init__(request, *args, **kwargs)
    
    @Device.confirmed.getter
    def is_confirmed(self):
        email_device = self.request.session.get('email_device')
        return email_device is not None and email_device['address'] == self.address and email_device['confirmed'] == True

    def generate_challenge(self):
        # current_site_name = get_current_site(self.request).name
        pincode = self.__generate_pincode()
        body = self.MESSAGE_TEMPLATE.format('یادسنج', pincode)
        subject = self.SUBJECT.format(pincode)
        # sender = SENDER
        recipient_list = [self.address]
        fail_silently = False
        send_mail(subject=subject, message=body, from_email=self.SENDER, recipient_list=recipient_list, fail_silently=fail_silently)
        self.__update_session(pincode=pincode, send_at = timezone.now())
        
    def __generate_pincode(self):
        return secrets.choice(range(10**(self.DIGITS-1), 10**(self.DIGITS)-1))

    def __update_session(self, pincode, send_at):
        self.request.session['email_device']={
            'address': self.address,
            'pincode': pincode,
            'send_at': datetime.datetime.timestamp(send_at),
            'confirmed': False
        }
        request.session.modified = True

    def verify_token(self, pincode):
        try:
            pincode = int(pincode)
        except:
            return False
        email_device = self.request.session.get('email_device')
        if email_device is not None and email_device['address'] == self.address and email_device['pincode'] == pincode:
            expire_at = email_device['send_at'] + self.EMAIL_TOKEN_EXPIRE_AFTER
            now = timezone.now().timestamp()
            if now < expire_at:
                self.request.session['email_device']['confirmed'] = True
                self.request.session.modified = True
                return True
        return False


class SMSDevice(Device):

    MESSAGE_TEMPLATE = 'کاربر گرامی! کد تایید موبایل شما در {0}، {1} می‌باشد.'
    DIGITS = getattr(settings, 'EMAIL_TOKEN_DIGITS', 6)
    SMS_TOKEN_EXPIRE_AFTER = getattr(settings, "SMS_TOKEN_EXPIRE_AFTER", 120)
    GATEWAY = getattr(settings, 'SMS_TOKEN_GATEWAY', 'default')
    SENDER = settings.SMS_GATEWAYS['default']['number']
    USERNAME = settings.SMS_GATEWAYS['default']['username']
    PASSWORD = settings.SMS_GATEWAYS['default']['password']
    # SENDER = getattr(settings, "SMS_TOKEN_SENDER",getattr(settings,'SMS_GATEWAYS')['default']['number'])
    # SEND_TOLERANCE = getattr(settings, 'EMAIL_TOKEN_SEND_TOLERANCE', 5)

    def __init__(self, request, number, *args, **kwargs):
        self.number = number
        super(SMSDevice, self).__init__(request, *args, **kwargs)

    @Device.confirmed.getter
    def is_confirmed(self):
        sms_device = self.request.session.get('sms_device')
        return sms_device is not None and sms_device['address'] == self.address and sms_device['confirmed'] == True

    def generate_challenge(self):
        # current_site_name = get_current_site(self.request).name
        pincode = self.__generate_pincode()
        message = self.MESSAGE_TEMPLATE.format('یادسنج', pincode)
        api = Api(self.USERNAME, self.PASSWORD)
        api.sms().send(self.number, self.SENDER, message)
        self.__update_session(pincode=pincode, send_at = timezone.now())
        
    def __generate_pincode(self):
        return secrets.choice(range(10**(self.DIGITS-1), 10**(self.DIGITS)-1))

    def __update_session(self, pincode, send_at):
        self.request.session['sms_device']={
            'number': self.number,
            'pincode': pincode,
            'send_at': datetime.datetime.timestamp(send_at),
            'confirmed': False
        }
        request.session.modified = True

    def verify_token(self, pincode):
        try:
            pincode = int(pincode)
        except:
            return False
        sms_device = self.request.session.get('sms_device')
        if sms_device is not None and sms_device['number'] == self.number and sms_device['pincode'] == pincode:
            expire_at = sms_device['send_at'] + self.SMS_TOKEN_EXPIRE_AFTER
            now = timezone.now().timestamp()
            if now < expire_at:
                self.request.session['sms_device']['confirmed'] = True
                request.session.modified = True
                return True
        return False

