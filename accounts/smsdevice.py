from django.db import models
from django.conf import settings
import secrets
from binascii import unhexlify

from sms import Api
from pyotp import TOTP
from pyotp.utils import hex_validator, random_hex
from .devices import Device

def default_key():
    return random_hex(20)


def key_validator(value):
    return hex_validator()(value)

# SMS_OTP_EXPIRE_AFTER = 120
# SMS_OTP_TOLERANCE_CHECK = 5
MESSAGE_TEMPLATE = 'کد فعال‌سازی شما {} است.'
class SMSDevice(Device):
    number = models.CharField(
        max_length = 10,
        help_text = "The mobile number to deliver tokens to."
    )
    key = models.CharField(
        max_length=80,
        validators=[key_validator],
        default=default_key,
        help_text='A hex-encoded secret key of up to 20 bytes.'
    )
    # token = models.CharField(
    #     max_length = 10,
    #     default = secrets.choice(range(100000, 1000000))
    # )
    # checked_times = models.PositiveSmallIntegerField(
    #     default = 0
    # )
    # expire_after = models.PositiveSmallIntegerField(
    #     default = settings.get('SMS_OTP_EXPIRE_AFTER') if settings.get('SMS_OTP_EXPIRE_AFTER') else  SMS_OTP_EXPIRATION_TIME
    # )
    @property
    def bin_key(self):
        return unhexlify(self.key.encode())
    
    def generate_challenge(self):
        token = TOTP(self.bin_key).now()
        message = MESSAGE_TEMPLATE.format(token)
        self._deliver_token(message)

    def _deliver_token(self, message):
        orig = settings.GATEWAYS['default']['orig']
        username = settings.GATEWAYS['default']['username']
        password = settings.GATEWAYS['default']['password']
        body = message
        api = Api(username,password)
        sms = api.sms()
        sms.send(self.number,orig,body)

    def verify_token(self, token):
        try:
            token = int(token)
        except Exception:
            verified = False
        else:
            verified = TOTP(self.bin_key).verify(token)

        return verified