from django.db import models
from django.core.validators import RegexValidator
from django.conf import settings
import datetime
class AbstractToken(models.Model):
    token = models.TextField()
    send_at = models.DateTimeField(default=timezone.now, editable=False)
    expire_at = models.DateTimeField(editable=False)
    verified_at = models.DateTimeField(editable=False, blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super(AbstractToken, self).__init__(*args, **kwargs)
        if not self.expire_at:
            self.expire_at = self.send_at + datetime.timedelta(seconds=settings.TOKEN_EXPIRATION_TIME)
    class Meta:
        abstract = True


class Token(AbstractToken):
    dest = models.TextField()


class PhoneToken(AbstractToken):
    dest = models.TextField(validators=[RegexValidator('^0?9\d{9}$')])
    def __init__(self, *args, **kwargs):
        super(PhoneToken, self).__init__(*args, **kwargs)
        if not self.expire_at:
            self.expire_at = self.send_at + datetime.timedelta(seconds=settings.PHONE_TOKEN_EXPIRATION_TIME)


class EmailToken(AbstractToken):
    dest = models.EmailField()
    def __init__(self, *args, **kwargs):
        super(EmailToken, self).__init__(*args, **kwargs)
        if not self.expire_at:
            self.expire_at = self.send_at + datetime.timedelta(seconds=settings.EMAIL_TOKEN_EXPIRATION_TIME)
