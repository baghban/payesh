from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import datetime
from sms import Api
from django.conf import settings
from django.db.models.manager import Manager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, phone_number, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email and not phone_number:
            raise ValueError('Users must have an email or phone_number')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, phone_number=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, phone_number, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, phone_number, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        blank=True,
        null=True,
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    email = models.EmailField(_('email address'), unique=True, blank=True, null=True)
    phone_number = models.CharField(
        unique=True,
        max_length=11,
        blank=True,null=True,
        validators=[RegexValidator(
            regex = '^0?9\d{9}$',
            message = _('Phone number is not valid')
        )]
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into tDateTihis admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now, editable=False)
    date_removed = models.DateTimeField(_('date removed'), blank=True, null=True, editable=False)zzzzzz

    objects = UserManager()

    
    USERNAME_FIELD = 'username'
    PHONENUMBER_FIELD = 'phone_number'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', 'phone_number']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)
        if not self.email and not self.phone_number:
            raise ValidationError(_('One of two fields email or phone number are required'))

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        if not self.email:
            return
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def sms_user(self, message, sender=settings.SMS_GATEWAYS['default']['number']):
        if not self.phone_number:
            return
        to = self.phone_number
        username = settings.SMS_GATEWAYS['default']['username']
        password = settings.SMS_GATEWAYS['default']['password']
        api = Api(username,password)
        sms = api.sms()
        sms.send(to, sender, message)


