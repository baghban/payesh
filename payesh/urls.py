"""payesh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
# from mytest import views as tviews
from accounts import views
from accounts.views import RegisterView
from accounts.views import LoginView
from home.views import HomeView

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('test', tviews.test, name='test'),
    path('', HomeView.as_view(), name='home'),
    path('register', RegisterView.as_view(), name='register'),
    path('login', LoginView.as_view(), name='login'),
    path('sendtoken', views.send_token, name="sendtoken"),
    path('validatephoneemail', views.validate_phone_email, name='validatephoneemail'),
    path('validatetoken', views.validate_token, name='validatetoken')
]
