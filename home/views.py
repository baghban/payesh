from django.shortcuts import render
from django.shortcuts import render, HttpResponse
from django.views import View

class HomeView(View):
    def get(self,request):
        if request.user.is_authenticated:
            text = "%s %s %s %s شما لاگین کرده‌اید." %(request.user.first_name, request.user.last_name, request.user.email, request.user.phone_number)
        else:
            text = "شما لاگین نکرده‌اید"
        return HttpResponse(text)
