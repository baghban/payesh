from django.apps import AppConfig


class PyotpConfig(AppConfig):
    name = 'pyotp'
