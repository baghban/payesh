from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.core.validators import MaxValueValidator, MinValueValidator

class EduStage(models.Model):
    name = models.CharField(_("name"), max_length=50)
    value = models.PositiveSmallIntegerField(_("value"))


class Grade(models.Model):
    name = models.CharField(_("name"), max_length=50)
    value = models.PositiveSmallIntegerField(_("value"))
    edu_stage = models.ForeignKey("EduStage", verbose_name=_("Educational Stage"), on_delete=models.CASCADE)


class Major(models.Model):
    name = models.CharField(_("name"), max_length=50)


class Book(models.Model):
    name = models.CharField(_("name"), max_length=50)
    grade = models.ForeignKey("Grade", verbose_name=_("grade"), on_delete=models.CASCADE)
    major = models.ForeignKey("Major", verbose_name=_("major"), on_delete=models.CASCADE)

    
class Season(models.Model):

    name = models.CharField(_("name"), max_length=50)
    value = models.PositiveSmallIntegerField(_("value"))
    book = models.ForeignKey("Book", verbose_name=_("book"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Season")
        verbose_name_plural = _("Season")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Season_detail", kwargs={"pk": self.pk})


class Lesson(models.Model):

    name = models.CharField(_("name"), max_length=50)
    value = models.PositiveSmallIntegerField(_("value"))
    season = models.ForeignKey("Season", verbose_name=_("season"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Lesson")
        verbose_name_plural = _("Lessons")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Lesson_detail", kwargs={"pk": self.pk})


class QuestionType(models.Model):

    name = models.CharField(_("name"), max_length=50)

    class Meta:
        verbose_name = _("QuestionType")
        verbose_name_plural = _("QuestionTypes")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("QuestionType_detail", kwargs={"pk": self.pk})



class Question(models.Model):

    DIFFICULTIES = [
        (1, _('very easy')),
        (2, _('easy')),
        (3, _('moderate')),
        (4, _('hard')),
        (5, _('very hard'))
    ]

    content = models.TextField(_("content"))
    point = models.FloatField(_("point"))
    difficulty = models.PositiveSmallIntegerField(
        _("difficulty"),
        choices=DIFFICULTIES,
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    type = models.ForeignKey("QuestionType", verbose_name=_("question type"), on_delete=models.CASCADE)
    season = models.ForeignKey("Season", verbose_name=_("season"), on_delete=models.CASCADE)
    lesson = models.ForeignKey("Lesson", verbose_name=_("lesson"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    def __str__(self):
        return self.content

    def get_absolute_url(self):
        return reverse("Question_detail", kwargs={"pk": self.pk})


